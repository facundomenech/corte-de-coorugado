/**
 * Nombre:   Arduino - Automatización Sistema de Corte
 * Autor:    Gonzalo Riera
 * Web:      www.dmasoluciones.net
 * Version:  2.0.21
 * Date:     2022/11/24
 *                       
 *NOTA: 1 VUELTA DE LA RUEDA = 600 PASOS (200 PASOS x 3[REL. DE TRANSMISION])
 *PASOS: 1mm = 3.183 PASOS
*/

/**
 *  LIBRERIAS NECESARIAS PARA EL FUNCIONAMIENTO DEL CODIGO
 */
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <EEPROM.h>
#include <ClickEncoder.h>
#include <TimerOne.h>
#include <AccelStepper.h>

/**
 *  OBJETOS DE LAS LIBRERIAS
 */
LiquidCrystal_I2C lcd(0x27, 20, 4);      //Configuracion del LCD I2C (puede ser necesario cambiar el primer valor con la direccion del LCD)
AccelStepper stepperZ(1, 2, 3);          // Configuración de AccelStepper Nema 17 (pin2 step, pin3 dir)
//AccelStepper stepperX(1, 5, 6);          // Configuración de AccelStepper Nema 23 (pin5 step, pin6 dir)

/**
 *  MACROS, CONSTANTES, ENUMERADORES, ESTRUCTURAS Y VARIABLES GLOBALES
 */
#define COUNT(x) sizeof(x)/sizeof(*x)                   // Macro para contar el numero de elementos de un array
const byte pinstepX   = 5;                              // pin stwep del nema 23
const byte pindirX    = 6;                              // pin dir del nema 23
const byte ENA        = 7;                              // Pin enable del Nema 23
const byte pENCO_SW   = 28;                             // Pin encoder SW
const byte pENCO_DT   = 32;                             // Pin encoder DT
const byte pENCO_CLK  = 30;                             // Pin encoder CLK
const byte btnAtras   = 34;                             // Pin botón "atras"
const byte btnStart   = 36;                             // Pin botón "start"
const byte swTapaG    = 13;                             // Pin switch tapa Guillotina
const byte swTapaA    = 40;                             // Pin switch tapa Arrastre
const byte swCero     = 42;                             // Pin switch cero apretador arrastre
const byte swMatriz   = 44;                             // Pin switch "matriz arriba" NO SE ESTÁ USANDO
const byte swCuchilla = 46;                             // Pin switch "cuchilla abajo" NO SE ESTÁ USANDO
const byte relMatriz  = 29;                             // Pin relé accionamiento matriz
const byte relCuchilla= 31;                             // Pin relé accionamiento cuchilla
const byte sensInfra  = 19;                             // Pin sensor Optico Infrarrojo
const byte ledCuchilla= 50;                             // Pin LED estado relay cuchilla
//const byte ledStepp23 = 52;                             // Pin LED pasos NEMA 23
const byte rowsLCD    = 4;                              // Filas del LCD
const byte columnsLCD = 20;                             // Columnas del LCD
const byte iARROW     = 0;                              // ID icono flecha
const byte bARROW[]   = {                               // Bits icono flecha
    B00000, B00100, B00110, B11111,
    B00110, B00100, B00000, B00000
};

bool pasoX = LOW; //contiene el estado del pin de paso
bool checkFirtPiece = false;  //Contiene el estado de chequeo de primera pieza para cada lote
bool whileFirtPiece = false;  //para while de control de primera pieza de lote
bool tookStep= false;    //bandera cuando dió un paso


bool prevStatusSens = false;                            //almacena el estado anterior del sensor infrarrojo
bool statusSensor;                                      //almacena el estado del sensor infrarrojo
long increasSens = 0;                                   //variable para almacenar la cantidad de crestas del corrugado
//bool entryPasoseis = false;                             //para marcar cuando entró al paso 6
bool entryOpenHoming = false;                           //para marcar cuando entró al homing

//TIMERS
unsigned long timeStatusSens = micros();                //variable para almacenar el momento en que se hizo la lectura anterior del sensor, para eliminar ruido (hay que ajustar)
//const int debounceTime = 5000;                         // REVISAR variable para definir el tiempo de antirrebote en microsegundos
unsigned long time_pasos = micros();                    //variable gestion de delay entre pasos
int cresta = 0;                                         //suma veces en cresta
int valle = 0;                                          //suma veces en valle
int delay_pasos=600;                                    //variable para definir el delay entre pasos, en microsegundos
float acel_pasos=1;                                     //para definir aceleracion
float desa_pasos=1;                                     //para desaceleracion
int puntero1 = 0;                                       //puntero para definir pocisión dentro de un vector.
const int muestras = 2;                                 //cantidad de muestras de estado del sensor
bool estadosSensor [muestras];                                //vector para almacenar estados del sensor

long crestasCal = 0;                                     //variable para almacenar cantidad de crestas en la calibracion
long lengthPipe = 0;                                     //variable para almacenar el largo medido en la calibracion
long lengthFirstPipe = 0;                                //variable para almacenar el largo medido en la primer pieza del lote
long targgetCrest = 0;                                   //variable para almacenar el numero de crestas que se debe desplazar para llegar al largo seteado
long previousLength = 0;                                 //variable para almacenar el largo seteado anterior
int cantLote=0;                                          //variable para el for que cuenta los lotes

bool statusForCut[8] = {true,true,false,true,true,true,true,true};                               //para definir si el corrugado en cuestion debe ser cortado cuando el sensor detecta cresta o valle (false o true)
long delay_pasos_extra=5000;                            //variable para definir el delay entre pasos extra, en microsegundos
int target_pasos_extra[8]={0,0,2,0,0,0,0,0};            //define cuantos pasos extra debe dar el motor antes de subir la matriz
int indice=0;                                           //para definir posicion dentro de los arrays segun sea el corrugado

int cont_pasos_crestas=0;                                    //contador de pasos totales de crestas en inicio
int cont_pasos_valles=0;                                     //contador de pasos totales de valles en inicio

int pasos_x_cresta=0;                                   //para almacenar el valor que se calcula de pasos por cresta
int pasos_x_valle=0;                                    //para almacenar el valor que se calcula de pasos por valle

int sumador_stepp = 0;                                   //variable para almacenar la cantidad de pasos que va dando en valles y crestas
long sumador_sensor = 0;                                   //variable para almacenar la cantidad de crestas y valles cuando corta lotes

#define ENCODER_STEPS_PER_NOTCH    4
ClickEncoder *encoder;
int16_t last, valorencoder;

enum Button{ Unknown, Ok, Atras } btnPressed;            // Enumerador con los diferentes botones disponibles
enum Screen{ Menu1, Menu2, Flag, Number, Metros };       // Enumerador con los distintos tipos de submenus disponibles

//unsigned long tiempo = millis(); //para delay

void timerIsr() {
  encoder->service();
}

const char *txMENU[] = {                                // Textos del menu principal
    "Diametro  ",
    "Largo [m] ",
    "Cantidad  ",
    "OK        "
 };
const byte iMENU = COUNT(txMENU);                       // Numero de items /opciones del menu principal

enum eSMENU1{ cuatrocinco, sietecinco, diez, trece, diecisiete, veintidos, veintiseis, veintinueve };         // Enumerador de las opciones disponibles del submenu 1 (tienen que seguir el mismo orden que los textos)
const char *txSMENU1[] = {                              // Textos del submenu 1
    "4,5",
    "7,5",
    "10 ",
    "13 ",
    "17 ",
    "22 ",
    "26 ",
    "29 "
};

// Variables del Stepper Apretador
long posTorn;                                // Usado para guardar el valor del recorrido para cada diámetro de corrugado
int move_finished=1;                         // Chequea si el movimiento se completó
long initial_homing=-1;                      // Usado para hacer el "Home" al inicio

/* ESTRUCTURAS CONFIGURACION */
struct MYDATA{                               // Estructura STRUCT con las variables que almacenaran los datos que se guardaran en la memoria EEPROM
    int initialized;
    int diametro;
    int largo;
    int cantidad;
};
union MEMORY{                                // Estructura UNION para facilitar la lectura y escritura en la EEPROM de la estructura STRUCT
    MYDATA d;
    byte b[sizeof(MYDATA)];
}
memory;

/**
 * INICIO Y CONFIGURACION DEL PROGRAMA
 */

void setup() {
//Serial.begin(115200);
Serial.println("SERIAL OK");
pinMode(pindirX, OUTPUT);
pinMode(pinstepX, OUTPUT);
pinMode(pENCO_SW,  INPUT_PULLUP);
pinMode(btnAtras,  INPUT_PULLUP);
pinMode(btnStart,  INPUT_PULLUP);
pinMode(swTapaG,  INPUT_PULLUP);
pinMode(swTapaA,  INPUT_PULLUP);
pinMode(swCero,  INPUT_PULLUP);
pinMode(swMatriz,  INPUT_PULLUP);
pinMode(swCuchilla,  INPUT_PULLUP);
pinMode(sensInfra,  INPUT_PULLUP);
pinMode(relMatriz,  OUTPUT);
pinMode(relCuchilla,  OUTPUT);
pinMode(ledCuchilla,  OUTPUT);  //Agregado por FD para emulacion
pinMode(ENA, OUTPUT);
digitalWrite(relMatriz, HIGH);
digitalWrite(relCuchilla, HIGH);
digitalWrite(ledCuchilla, HIGH);
digitalWrite(ENA, LOW);
digitalWrite(pindirX, HIGH);   //define direccion de giro del motor nema23
Serial.println("INICIALIZACION DE PINES OK");

//Inicializacion de interrupcion sensor optico
prevStatusSens = digitalRead(sensInfra);  //almacena el estado inicial del sensor
//attachInterrupt(digitalPinToInterrupt(sensInfra), countsens, CHANGE);

// Inicia el LCD:
  lcd.init();
  lcd.backlight();
  lcd.begin(columnsLCD, rowsLCD);
  lcd.createChar(iARROW, bARROW);
  Serial.println("LCD INICIADO");

    // Imprime la informacion del proyecto:
  lcd.setCursor(0,0); lcd.print("  CORTE CORRUGADO   ");
  lcd.setCursor(0,2); lcd.print("<Ver 2.0.21 20221124>");
  lcd.setCursor(0,3); lcd.print(" Inicializando....  ");
  delay (2000);

  bool initVector=false;  //para inicializar vector
  for (int i=0; i<=(muestras-1); i++){
    estadosSensor [i]=initVector;
    initVector=!initVector;    
  }


     
     // Inicio Clickencoder y TimerOne
  encoder = new ClickEncoder(pENCO_DT, pENCO_CLK, pENCO_SW, ENCODER_STEPS_PER_NOTCH);
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);     //comentado solo para test
  last = -1;

    // Carga la configuracion de la EEPROM, y la configura la primera vez: BAJADO POR FD
 readConfiguration();
 Serial.println("FIN READ CONFIGURATION"); 

     //  Configura la velocidad y aceleración máximas del StepperZ para el "Homing"
  stepperZ.setMaxSpeed(850.0);
  stepperZ.setAcceleration(500.0); 

     // Inicia el Homing del Apretador
  while (digitalRead(swCero)) {             // rota en sentido contrario hasta que el switch se active   
    stepperZ.moveTo(initial_homing);        // Configura la posición hacia la que se mueve
    initial_homing--;                       // Decrease by 1 for next move if needed
    stepperZ.run();                         // Start moving the stepper
}
  stepperZ.setCurrentPosition(0);           // Set the current position as zero for now
  stepperZ.setMaxSpeed(300.0);              // Set Max Speed of Stepper (Slower to get better accuracy)
  stepperZ.setAcceleration(300.0);          // Set Acceleration of Stepper
  initial_homing=1;
  while (!digitalRead(swCero)) {            // Make the Stepper move CW until the switch is deactivated
    stepperZ.moveTo(initial_homing);  
    stepperZ.run();
    initial_homing++;
  }
  stepperZ.setCurrentPosition(0);
  stepperZ.setMaxSpeed(1000.0);      // Set Max Speed of Stepper (Faster for regular movements)
  stepperZ.setAcceleration(1000.0);  // Set Acceleration of Stepper

       //  Configura la velocidad y aceleración máximas del StepperX para el Arrastre
  //stepperX.setMinPulseWidth(20);
  //stepperX.setMaxSpeed(400.0);
  //stepperX.setAcceleration(2000.0); 
  lcd.clear();

  //tiempo = millis(); //para delay
}


/**
 * PROGRAMA PRINCIPAL
 */
void loop() {
  Serial.println("En el LOOP"); //for debug
  lcd.setCursor(0,0);
  lcd.print("   INGRESE DATOS    ");
  lcd.setCursor(0,1);
  lcd.print("    NUEVO LOTE      ");
  lcd.setCursor(0,3);
  lcd.print("    Presione <START>");
   btnPressed = readButtons();
   if( btnPressed == Button::Ok)
     openMenu();
}

void openPasocero() {
    Serial.println("PASO CERO");
    boolean exitMenu   = false;
    boolean forcePrint = true;
    lcd.clear();
    lcd.setCursor(0,0); lcd.print("Retire el Corrugado ");
    lcd.setCursor(0,1); lcd.print("Existente           ");
    lcd.setCursor(0,3); lcd.print("Luego presione START");
    delay(100);
    while( !exitMenu ) {
      btnPressed = readButtons();      
      if( btnPressed == Button::Ok ) {
       openPasouno();
      }
      if( btnPressed == Button::Atras ) {
       openMenu();
      }
   }
}

void openPasouno() {
    boolean exitMenu   = false;
    boolean forcePrint = true;
    lcd.clear();
    lcd.setCursor(0,0); lcd.print("Suba tapa Guillotina");
    lcd.setCursor(0,1); lcd.print("Coloque accesorios  ");
    lcd.setCursor(0,2); lcd.print("para diametro       ");
    lcd.setCursor(0,3); lcd.print("Luego presione START");
    switch( memory.d.diametro ) {
       case eSMENU1::cuatrocinco:
           lcd.setCursor(15,2); lcd.print("4,5"); posTorn = 47; indice=0; break;
       case eSMENU1::sietecinco:
           lcd.setCursor(15,2); lcd.print("7,5"); posTorn = 44; indice=1; break;
       case eSMENU1::diez:
           lcd.setCursor(15,2); lcd.print("10");  posTorn = 40; indice=2; break;
       case eSMENU1::trece:
           lcd.setCursor(15,2); lcd.print("13");  posTorn = 38; indice=3; break;
       case eSMENU1::diecisiete:
           lcd.setCursor(15,2); lcd.print("17");  posTorn = 34; indice=4; break;
       case eSMENU1::veintidos:
           lcd.setCursor(15,2); lcd.print("22");  posTorn = 31; indice=5; break;
       case eSMENU1::veintiseis:
           lcd.setCursor(15,2); lcd.print("26");  posTorn = 25; indice=6; break;
       case eSMENU1::veintinueve:
           lcd.setCursor(15,2); lcd.print("29");  posTorn = 22; indice=7; break;
      }
      while( !exitMenu ) {
       btnPressed = readButtons();
       int TapaG = digitalRead(swTapaG);  //lee pin tapa guillotina
        if(TapaG == HIGH) {
          digitalWrite(relMatriz, LOW);
        }
        if( btnPressed == Button::Ok && TapaG == HIGH) // && digitalRead(swMatriz) == LOW)
         openPasodos();
        if( btnPressed == Button::Atras ) {
         openPasocero();
        }
    }
}         

void openPasodos() {
    boolean exitMenu   = false;
    boolean forcePrint = true;
    lcd.clear();
    lcd.setCursor(0,0); lcd.print("Baje tapa Guillotina");
    lcd.setCursor(0,3); lcd.print("Luego presione START");
    while( !exitMenu ) {
     btnPressed = readButtons();
     int TapaG = digitalRead(swTapaG);
      if( btnPressed == Button::Ok && TapaG == LOW) {
        digitalWrite(relMatriz, HIGH);
        openPasotres();
      }
      if( btnPressed == Button::Atras ) {
       openPasouno();
    }
  }
}

void openPasotres() {
    boolean exitMenu   = false;
    boolean forcePrint = true;
    lcd.clear();
    lcd.setCursor(0,0); lcd.print("Suba tapa Arrastre  ");
    lcd.setCursor(0,1); lcd.print("Pase el tubo hasta  ");
    lcd.setCursor(0,2); lcd.print("final de Guillotina ");
    lcd.setCursor(0,3); lcd.print("Luego presione START");
    while( !exitMenu ) {
     btnPressed = readButtons();
     int TapaA = digitalRead(swTapaA);
      if( btnPressed == Button::Ok && TapaA == HIGH)
       openPasocuatro();
      if( btnPressed == Button::Atras ) {
       openPasodos();
      }
    }
}

void openPasocuatro() {
    boolean exitMenu   = false;
    boolean forcePrint = true;
    lcd.clear();
    lcd.setCursor(0,0); lcd.print("Baje tapa Arrastre  ");
    lcd.setCursor(0,1); lcd.print("Espere que corte    ");
    lcd.setCursor(0,2); lcd.print("Retire el sobrante  ");
    lcd.setCursor(0,3); lcd.print("Luego presione START");
    while( !exitMenu ) {  
     Serial.println("ESPERANDO CIERRE DE TAPA ARRASTRE");            
     int TapaA = digitalRead(swTapaA);
     if( TapaA == LOW) {
     openHoming();
     }
   }
}


//Paso CINCO en pestaña aparte

void openPasoseis() {
  
    boolean exitMenu   = false;
    boolean forcePrint = true;
    delay(1000);
    lcd.clear();
    lcd.setCursor(0,0); lcd.print("   LOTE COMPLETO    ");
    lcd.setCursor(0,1); lcd.print("Repetir    <VERDE>  ");
    lcd.setCursor(0,2); lcd.print("Volver     <ROJO>   ");
    lcd.setCursor(0,3); lcd.print("Presione Verde/Rojo ");
    while( !exitMenu ) {
     btnPressed = readButtons();
     if( btnPressed == Button::Ok ) {
       openPasocinco();
     }
       else if( btnPressed == Button::Atras ) {
        stepperZ.runToNewPosition(0);
        lcd.clear();        
        openMenu();        
       }
    }
    //entryPasoseis = true;
    entryOpenHoming = false;     
}

 //Funcion eStop
void eStop() {
  digitalWrite(ENA, LOW);
  digitalWrite(relCuchilla, HIGH);
  digitalWrite(ledCuchilla, HIGH);            //muestra estado cuchilla
  digitalWrite(relMatriz, HIGH);
  boolean exitMenu   = false;
  lcd.clear();
  lcd.setCursor(0,0); lcd.print("   | DETENIDO |     ");
  lcd.setCursor(0,1); lcd.print("Continuar  <VERDE>  ");
  lcd.setCursor(0,2); lcd.print("Reiniciar  <ROJO>   ");
  lcd.setCursor(0,3); lcd.print("Presione Verde/Rojo ");
  while( !exitMenu ) {
   btnPressed = readButtons();
   int TapaA = digitalRead(swTapaA);
   int TapaG = digitalRead(swTapaG);
   if( btnPressed == Button::Ok && TapaA == LOW && TapaG == LOW ) {
    digitalWrite(ENA, HIGH);
    lcd.clear();
    return;
   }
   else if( btnPressed == Button::Atras ) {
      stepperZ.runToNewPosition(0);
      lcd.clear();        
      openMenu();        
     }
  }       
}
    
/**
 *  LEE (Y CONFIGURA LA PRIMERA VEZ) LA MEMORIA EEPROM CON LA CONFIGURACION DE USUARIO
 */
void readConfiguration() {
    for( int i=0 ; i < sizeof(memory.d) ; i++  )
        memory.b[i] = EEPROM.read(i);    
    if( memory.d.initialized != 'Y' ) {
        Serial.println("EEPROM NO INICIALIZADA, DEBE INICIALIZARCE"); //for debug
        memory.d.initialized = 'Y';
        memory.d.diametro    = 1;
        memory.d.largo       = 0;
        memory.d.cantidad    = 0;
        writeConfiguration();
    }
    Serial.println("LECTURA EEPROM OK");
}

/**
 *  ESCRIBE LA MEMORIA EEPROM CON AL CONFIGURACION DE USUARIO
 */
void writeConfiguration() {
    for( int i=0 ; i<sizeof(memory.d) ; i++  ) {
        EEPROM.write( i, memory.b[i] );
        //openPasocero(); //modificado por FD
    }
    Serial.println("ESCRITURA EEPROM OK");
    openPasocero(); //antes estaba en el for
}


/**
 *  LEE LOS DISTINTOS BOTONES DISPONIBLES Y DEVUELVE EL QUE HAYA SIDO PULSADO
 *      Este bloque de codigo varia dependiendo del tipo de teclado conectado, en mi blog estan
 *      disponibles los ejemplos para teclados digitales, analogicos, y este para encoder rotatorio.
 *      Al cambiar de tipo de teclado hay que adaptar tambien el resto de codigo para que haga uso de cada boton segun queramos.
 */
Button readButtons() {
    btnPressed = Button::Unknown;
     if( !digitalRead(pENCO_SW) || !digitalRead(btnStart)) {
        while(!digitalRead(pENCO_SW) || !digitalRead(btnStart));
        btnPressed = Button::Ok;
        delay(50);
       }
     if( !digitalRead(btnAtras) ) {
        while(!digitalRead(btnAtras));
        btnPressed = Button::Atras;
        delay(50);
       }
    return btnPressed;
}

void cut() {
  //Verifica que la cresta esté en posición, si no lo está, la posiciona
  while(digitalRead(sensInfra)==!statusForCut[indice]){
        
    Serial.println("REACOMODO");
    if (time_pasos<(micros()-delay_pasos)){
      stepper();
    } 
  }

  //Añade pasos extra harcodeados para ajustar pocision fina de la cresta para que coincida con la matriz
  int pasosextra=0;
  while(pasosextra<target_pasos_extra[indice]){
    //countsens();
    Serial.println("PASOS EXTRA");
    if (time_pasos<(micros()-delay_pasos_extra)){
      pasosextra++;
      stepper();
    } 
  }
  Serial.println("ACOMODO OK");
    
  // Rutina para cortar
  Serial.println("INICIO DE CORTE");
  //digitalWrite(ENA, LOW);   //libera motor de traccion
  //delay(200);      
  digitalWrite(relMatriz, LOW);                //Sube la matriz
  delay(600);      
  digitalWrite(relCuchilla, LOW);            //Baja la cuchilla
  delay(350);      
  digitalWrite(relCuchilla, HIGH);            //Sube la cuchilla
  delay(20);
  digitalWrite(relMatriz, HIGH);              //Baja la matriz
  delay(50);
  digitalWrite(ENA, HIGH);    //traba motor de traccion
  //fin rutina cortar
}

void countsens() {    //para contar cantidad de crestas del corrugado
  if (puntero1>(muestras-1)){
    puntero1=0;
  }

  if (digitalRead(sensInfra)){  //si lee cresta
    estadosSensor [puntero1] = true;
  }
  if (!digitalRead(sensInfra)){  //si lee valle
    estadosSensor [puntero1] = false;
  }
  puntero1++;

  cresta=0;
  valle=0;

  for (int i=0; i<=(muestras-1); i++){
    if (estadosSensor [i]){
      cresta++;
    }
    else{
      valle++;
    }
    //Serial.println("SENSOR " + String (estadosSensor [i]));  //QUITAR
  }

  //Serial.println("Crestas " + String (cresta));  //QUITAR
  //Serial.println("Valles  " + String (valle));  //QUITAR

  
  if (cresta == muestras){  //si estuvo x veces en cresta
    statusSensor=true;
  }

  if (valle == muestras){   //si estuvo x veces en valle
    statusSensor=false;
  }  

  if (statusSensor != prevStatusSens){  //si cambió las muestras del sensor
    prevStatusSens = statusSensor;      //cambia el estado del previo al actual
    increasSens++;  //suma cresta
    
  }
  tookStep= false;    //vuelve a bajar la bandera indicadora de que se dió un paso

}

void openPasocinco() { //Rutina de ejecución de trabajo de corte
    Timer1.detachInterrupt(); //para interrupciones de TimerONE FD
    boolean exitMenu   = false;
    boolean forcePrint = true;
    bool exitPasoCinco = false;   //para salir de corte de lote por parada de usuario

    if (previousLength != memory.d.largo){
      openHoming();
    }

    if (entryOpenHoming){
      //calculo de cantidad de crestas que tienen que pasar para el largo deseado
      targgetCrest = ((long(memory.d.largo *5) * crestasCal)/lengthPipe);   
      //fin cálculo
    }
    
    lcd.clear();
    lcd.setCursor(0,0); lcd.print("  LOTE EN PROGRESO  ");
    lcd.setCursor(0,1); lcd.print("Total:              ");
    lcd.setCursor(16,1);lcd.print(memory.d.cantidad);
    lcd.setCursor(0,2); lcd.print("Terminados:     0   ");
    lcd.setCursor(0,3); lcd.print("Restantes:          ");
    lcd.setCursor(16,3);lcd.print(memory.d.cantidad);    
    
    
    
    //boocle de corte de lote segun cantidad 
    cantLote=0;
    checkFirtPiece=false;  
    while ((cantLote<memory.d.cantidad)&&(!exitPasoCinco)) {  //antes era for     

      //Rutina para avanzar LARGO REQUERIDO ("memory.d.largo")
      
      digitalWrite(ENA, HIGH);  //habilita motor
      delay(100);      
      Serial.println("INICIO DE AVANCE PARA CORTE");
      

      increasSens = 0;  //pone a 0 la variable antes de comenzar a avanzar FD
      
      time_pasos = micros();

      bool finalizar = false;   //para parar corte de lote por parada de usuario
            
      while((increasSens<(targgetCrest))&&(!exitPasoCinco)) {  //debe salir de while cuando termina de contar las crestas
        if (tookStep){
          countsens();
        }
        if (time_pasos<(micros()-delay_pasos)){          
          btnPressed = readButtons(); //lee si se presionó un boton
          //Serial.println("CUENTA" + String (increasSens));  //QUITAR
          stepper();         
        }

        if (digitalRead(swTapaG) == HIGH || digitalRead(swTapaA) == HIGH || btnPressed == Button::Atras) {
            //stepperX.stop();
            //eStop();
            finalizar=true;  //finaliza el while
            delay (500);
            //break;
            
          }

          while (finalizar){
            btnPressed = readButtons(); //lee si se presionó un boton
            if(btnPressed == Button::Atras){
              exitPasoCinco = true; //para salir del paso cinco
              finalizar = false; //para salir de este while y retomar paso cinco
            }
            if( btnPressed == Button::Ok ){
              finalizar = false; //para salir de este while y retomar paso cinco
            }
          }

        
      }

      
      
      
      //if ((finalizado == 0) && (increasSens>=targgetCrest)) {
        Serial.println("FINALIZADO");
        //stepperX.stop();
        //finalizado = 1;
      //}
     
      
      // CORTAR
      cut();
      
      lcd.setCursor(0,0); lcd.print("  LOTE EN PROGRESO  ");
      lcd.setCursor(0,1); lcd.print("Total:              ");
      lcd.setCursor(16,1);lcd.print(memory.d.cantidad);
      lcd.setCursor(0,2); lcd.print("Terminados:         ");
      lcd.setCursor(0,3); lcd.print("Restantes:          ");
      lcd.setCursor(16,3);lcd.print(memory.d.cantidad);lcd.print("  ");
      lcd.setCursor(16,2); lcd.print(cantLote+1);
      lcd.setCursor(16,3); lcd.print(memory.d.cantidad - cantLote - 1);lcd.print("  ");
      delay(500);

      //verifica largo de primer pieza
      cantLote++; //suma para el for
      valFirtPiece(); //funcion que valida el largo de la primer pieza del lote
      
      
    }
    
    Timer1.attachInterrupt(timerIsr);     //vuelve a iniciar interrupciones de TimerONE FD    
    digitalWrite(ENA, LOW);
    openPasoseis();
}

/**
 *  MUESTRA EL MENU PRINCIPAL EN EL LCD.
 */
 void openMenu() {
    byte idxMenu       = 0;
    boolean exitMenu   = false;
    boolean forcePrint = true;
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("   INGRESE DATOS    ");
    delay(500);
    lcd.clear();
    while( !exitMenu ) {
      valorencoder += encoder->getValue();
      btnPressed = readButtons();
      if (valorencoder != last) {
        last = valorencoder;
          if (valorencoder >= iMENU-1) {valorencoder = iMENU-1;}
          if (valorencoder <= 0) {valorencoder = 0;}
          idxMenu = valorencoder;
          }
          else if( btnPressed == Button::Ok ) {
            switch( idxMenu ) {
                case 0: openSubMenu( idxMenu, Screen::Menu1,  &memory.d.diametro,    0, COUNT(txSMENU1)-1 ); break;
                case 1: openSubMenu( idxMenu, Screen::Metros, &memory.d.largo,       0, 15000             ); break;
                case 2: openSubMenu( idxMenu, Screen::Number, &memory.d.cantidad,    0, 100               ); break;
                case 3: writeConfiguration();                                                                break; //Salir y guardar
                }
            forcePrint = true;
            valorencoder = idxMenu;
        }
        if( !exitMenu && (forcePrint || valorencoder == last) ) {
            forcePrint = false;
            static const byte endFor1 = (iMENU+rowsLCD-1)/rowsLCD;
            int graphMenu     = 0;
            for( int i=1 ; i<=endFor1 ; i++ ) {
                if( idxMenu < i*rowsLCD ) {
                    graphMenu = (i-1) * rowsLCD;
                    break;
                }
            }
            byte endFor2 = graphMenu+rowsLCD;
            for( int i=graphMenu, j=0; i< endFor2 ; i++, j++ ) {
                lcd.setCursor(1, j);
                lcd.print( (i<iMENU) ? txMENU[i] : "                    " );
            }
            for( int i=0 ; i<rowsLCD ; i++ ) {
                lcd.setCursor(0, i);
                lcd.print(" ");
            }
            lcd.setCursor(0, idxMenu % rowsLCD );
            lcd.write(iARROW);
        }
    }
}

/**
 * MUESTRA EL SUBMENU EN EL LCD.
 *
 * @param menuID    ID del menu principal para usarlo como titulo del submenu
 * @param screen    Segun el tipo, se representara el submenu de una forma u otra.
 * @param value     Puntero a la variable que almacena el dato, y que se modificara.
 * @param minValue  Valor minimo que puede tener la variable.
 * @param maxValue  Valor maximo que puede tener la variable.
 */
void openSubMenu( byte menuID, Screen screen, int *value, int minValue, int maxValue ) {
    boolean exitSubMenu = false;
    boolean forcePrint  = true;
    while( !exitSubMenu ) {
       valorencoder += encoder->getValue();
       btnPressed = readButtons();
        if( btnPressed == Button::Ok ) {
            exitSubMenu = true;
        }
       if (valorencoder != last) {
        last = valorencoder;
         if (valorencoder >= maxValue) {valorencoder = maxValue;}
         if (valorencoder <= minValue) {valorencoder = minValue;}
          *value = valorencoder;
         }
        if( !exitSubMenu ) {
            forcePrint = false;
            
            if( screen == Screen::Menu1 ) {
              lcd.setCursor(columnsLCD-8, 0);
              lcd.print("<");
              lcd.setCursor(columnsLCD-1, 0);
              lcd.print(">");
              lcd.setCursor(columnsLCD-5, 0);
              lcd.print(txSMENU1[*value]);
            }
            else if( screen == Screen::Metros ) {
              lcd.setCursor(columnsLCD-9, 1);
              lcd.print("<");
              lcd.setCursor(columnsLCD-1, 1);
              lcd.print(">");
              lcd.setCursor(columnsLCD-7, 1);
              lcd.print(*value*.005,3);
              lcd.print(" ");
            }
            else if( screen == Screen::Number ) {
              lcd.setCursor(columnsLCD-8, 2);
              lcd.print("<");
              lcd.setCursor(columnsLCD-1, 2);
              lcd.print(">");
              lcd.setCursor(columnsLCD-5, 2);
              lcd.print(*value);
              lcd.print(" ");
            }
           valorencoder = *value;
        }
    }
}

void openHoming() {
    previousLength = memory.d.largo;
    boolean exitMenu   = false;  //estaba en false, cambiado para pruebas por FD
    boolean forcePrint = true;
    bool end_move = false;  //en true indica que terminó de mover
    delay(100);
    int crestas_ini = 30;   //cantidad de crestas para cortar primera vez
    int crestas_cal = (2*148); //590;   //cantidad de crestas para cortar para calibrar 148=250mm
        
        //Rutina para bajar apretador hasta diámetro correspondiente
      move_finished=0;                            // Set variable for checking move of the Stepper
      int carreraZ = posTorn * 200;
      while(move_finished != 1) {
      //Serial.println("BAJANDO APRETADOR");  //QUITAR
      stepperZ.setMaxSpeed(1000.0);      // Set Max Speed of Stepper (Faster for regular movements)
      stepperZ.setAcceleration(1000.0);  // Set Acceleration of Stepper
      stepperZ.moveTo(carreraZ);          // Set new moveto position of Stepper
      if ((stepperZ.distanceToGo() != 0)) {     // Check if the Stepper has reached desired position    
         stepperZ.run();                          // Move Stepper into position    
        }
        if ((move_finished == 0) && (stepperZ.distanceToGo() == 0)){
        //if ((move_finished == 0) && ((stepperZ.distanceToGo() == 0) || digitalRead(swCero))) {  // If move is completed Agregado por FD (antes: if ((move_finished == 0) && (stepperZ.distanceToGo() == 0))  )
          move_finished=1;                                             // Reset move variable
        }
      }

      
      //Rutina para avanzar primera vez DESPUNTE
      Timer1.detachInterrupt(); //para interrupciones de TimerONE FD      
      digitalWrite(ENA, HIGH);  //habilita motor
      delay(300);      
      Serial.println("INICIO DE AVANCE");
      //delay(300);

      increasSens = 0;  //pone a 0 la variable antes de comenzar a avanzar FD
      
      time_pasos = micros();
           
      while(increasSens<crestas_ini) {  //debe salir de while cuando termina de contar las crestas
        if (tookStep){
          countsens();
        }
        
        if ((time_pasos+delay_pasos)<micros()){
         //Serial.println("CUENTA" + String (increasSens));  //QUITAR
         //Serial.println("Crestas " + String (cresta));  //QUITAR
         //Serial.println("Valles  " + String (valle));  //QUITAR
          
          stepper();
          
        }
        
      }
            
      // CORTAR
      cut();
      
      
      //Rutina para avanzar largo de calibracion
      digitalWrite(ENA, HIGH);  //habilita motor
      delay(300);      
      Serial.println("INICIO DE AVANCE CAL");

      increasSens = 0;  //pone a 0 la variable antes de comenzar a avanzar FD
      
      time_pasos = micros();
            
      while(increasSens<=(crestas_cal)) {  //debe salir de while cuando termina de contar las crestas
        if (tookStep){
          countsens();
        }
        if ((time_pasos+delay_pasos)<micros()){
          //Serial.println("CUENTA" + String (increasSens));  //QUITAR
          stepper();
        }        
      }
      crestasCal = increasSens;   //REVISAR!!
            
      // Cortar
      cut();

      //Serial.println("pasos_crestas: "+String(cont_pasos_crestas));
      //Serial.println("pasos_valles: "+String(cont_pasos_valles));

      //pasos_x_cresta= (cont_pasos_crestas/crestas_cal)*90/100;
      //pasos_x_valle= (cont_pasos_valles/crestas_cal)*90/100;
      //crestasCal = 31;  //hardcodeado por ahora... luego dejar: crestasCal = increasSens;

      lcd.clear();
      lcd.setCursor(0,0); lcd.print("Mida el Corrugado   ");
      lcd.setCursor(0,1); lcd.print("Ingrese el valor    ");
      lcd.setCursor(0,2); lcd.print("Medida en mm: ");
      valorencoder = 500; //presetea en 1000mm
      lcd.setCursor(0,3); lcd.print("Luego presione START");

      //digitalWrite(ENA, LOW); //desactiva enable nema23
      Timer1.attachInterrupt(timerIsr);     //vuelve a iniciar interrupciones de TimerONE FD
        
    while( !exitMenu ) {
      
     valorencoder += encoder->getValue();
     lengthPipe = valorencoder;
     lcd.setCursor(14,2); lcd.print(String (lengthPipe)+ "   ");
     //Serial.println("ATRAPADO FIN RUTINA CIERRE DE TAPA, PRESS START");              
     btnPressed = readButtons();
     int TapaA = digitalRead(swTapaA);
     if( btnPressed == Button::Ok && TapaA == LOW){
      entryOpenHoming = true;       
      openPasocinco();      
     }
               
      if( btnPressed == Button::Atras ) {
       openPasotres();
      }
    }
    Timer1.detachInterrupt(); //para interrupciones de TimerONE FD
}

void stepper(){
  
  digitalWrite(pinstepX, pasoX);  //establece estado del pin
  time_pasos = micros();
  pasoX=!pasoX; //invierte estado para paso
  tookStep=true;
}

void valFirtPiece(){
  if (!checkFirtPiece){
    lcd.clear();
    lcd.setCursor(0,0); lcd.print("Mida el Corrugado   ");
    lcd.setCursor(0,1); lcd.print("Ingrese el valor    ");
    lcd.setCursor(0,2); lcd.print("Medida en mm: ");
    valorencoder =(memory.d.largo *5); //presetea en largo de pieza deseado
    lcd.setCursor(0,3); lcd.print("Luego presione START");

    //digitalWrite(ENA, LOW); //desactiva enable nema23
    Timer1.attachInterrupt(timerIsr);     //vuelve a iniciar interrupciones de TimerONE FD

    whileFirtPiece=false; //para que entre al while
    while( !whileFirtPiece) {
       
       valorencoder += encoder->getValue();
       lengthFirstPipe = valorencoder;
       lcd.setCursor(14,2); lcd.print(String (lengthFirstPipe)+ "   ");
       //Serial.println("ATRAPADO FIN RUTINA CIERRE DE TAPA, PRESS START");              
       btnPressed = readButtons();
       int TapaA = digitalRead(swTapaA);
       if( btnPressed == Button::Ok && TapaA == LOW){
          Timer1.detachInterrupt(); //para interrupciones de TimerONE FD       
          whileFirtPiece=true;      //para salir del while, REVISAR
       }
                 
       if( btnPressed == Button::Atras ) {
          openPasotres();
       }
    }
    

    //si la primer pieza tiene el largo correcto, la toma como del lote,
    if ((lengthFirstPipe==(memory.d.largo *5))||((lengthFirstPipe>(memory.d.largo *5))&&(lengthFirstPipe<((memory.d.largo *5)+(((memory.d.largo *5)*5)/100))))) { //si es igual que el target o mayor pero no mas de un 2% 
      checkFirtPiece=true;
      lcd.clear();
      lcd.setCursor(0,0); lcd.print("Largo OK            ");
      Timer1.detachInterrupt(); //para interrupciones de TimerONE FD
      
    }
    //sino no la toma como del lote
    else{
      checkFirtPiece=false; //para que vuelfa a entrar al if
      cantLote--; //para que no sume en el for
      lcd.clear();
      lcd.setCursor(0,0); lcd.print("DESCARTE EL CORRUGAD");
      lcd.setCursor(0,1); lcd.print("QUE ACABA DE MEDIR  ");
      lcd.setCursor(0,3); lcd.print("Luego presione START");

      Timer1.attachInterrupt(timerIsr);     //vuelve a iniciar interrupciones de TimerONE FD
      bool forWhile = false;  //para permanecer en el while posterior
      while( !forWhile) {
        
        btnPressed = readButtons();      
        if( btnPressed == Button::Ok ) {
          forWhile=true;
        }
        if( btnPressed == Button::Atras ) {
          openMenu();
        }
      }
      Timer1.detachInterrupt(); //para interrupciones de TimerONE FD

      //ajusta nuevo largo

      crestasCal=targgetCrest;

      targgetCrest = ((long(memory.d.largo *5) * crestasCal)/lengthFirstPipe); 
      
      //if (lengthFirstPipe<lengthPipe) {
        //long delta = abs(lengthFirstPipe-lengthPipe)
      //}
    }      
      
  }
      
}
